﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Server.Api.Entities;

namespace Server.Api.Database
{
    public class RefreshTokenRepository : IDisposable
    {
        private readonly ApplicationDbContext _db;

        public RefreshTokenRepository(ApplicationDbContext context)
        {
            _db = context;
        }

        public async Task<bool> Add(RefreshToken token)
        {
            var existingToken = _db.RefreshTokens.SingleOrDefault(r => r.Subject == token.Subject);

            if (existingToken != null)
                await Remove(existingToken);

            _db.RefreshTokens.Add(token);

            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> Remove(string id)
        {
            var refreshToken = await _db.RefreshTokens.FindAsync(id);

            if (refreshToken == null) return false;

            _db.RefreshTokens.Remove(refreshToken);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> Remove(RefreshToken refreshToken)
        {
            _db.RefreshTokens.Remove(refreshToken);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveByUserId(string userName)
        {
            var refreshToken = await _db.RefreshTokens.FirstOrDefaultAsync(token => token.Subject == userName);

            if (refreshToken == null) return false;

            _db.RefreshTokens.Remove(refreshToken);
            return await _db.SaveChangesAsync() > 0;
        }

        public async Task<RefreshToken> Find(string refreshTokenId)
            => await _db.RefreshTokens.FindAsync(refreshTokenId);

        public List<RefreshToken> GetAll() => _db.RefreshTokens.ToList();

        public void Dispose() => _db.Dispose();
    }
}