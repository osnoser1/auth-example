﻿namespace Server.Api.Core.Auth
{
    public class Credentials
    {
        public string grant_type { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string refresh_token { get; set; }
    }
}