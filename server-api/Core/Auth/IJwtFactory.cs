﻿using System.Collections.Generic;
using System.Security.Claims;

namespace Server.Api.Core.Auth
{
    public interface IJwtFactory
    {
        string GenerateEncodedToken(string username, ClaimsIdentity identity);
        ClaimsIdentity GenerateClaimsIdentity<T>(string username, T id, IEnumerable<string> roles);
    }
}