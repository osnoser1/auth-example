﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Server.Api.Core.Config;

namespace Server.Api.Core.Auth
{
    public class JwtFactory : IJwtFactory
    {
        private readonly AuthConfiguration _authConfiguration;

        public JwtFactory(IOptions<AuthConfiguration> authOptions)
        {
            _authConfiguration = authOptions.Value;
        }

        public string GenerateEncodedToken(string username, ClaimsIdentity identity)
        {
            var now = DateTime.UtcNow;

            var claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iat, now.ToString(CultureInfo.InvariantCulture),
                        ClaimValueTypes.Integer64),
                    //identity.FindFirst(Helpers.Constants.Strings.JwtClaimIdentifiers.Id)
                }
                .Concat(identity.FindAll(CustomClaimTypes.Role))
                .ToList();

            var symmetricKeyAsBase64 = _authConfiguration.SecretKey;
            var keyByteArray = Encoding.ASCII.GetBytes(symmetricKeyAsBase64);
            var signingKey = new SymmetricSecurityKey(keyByteArray);


            // Create the JWT security token and encode it.
            var jwt = new JwtSecurityToken(
                issuer: _authConfiguration.Issuer,
                audience: _authConfiguration.Audience,
                claims: claims,
                notBefore: now,
                expires: now.AddMinutes(_authConfiguration.JwtTokenLifeTime),
                signingCredentials: new SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256));

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);

            return encodedJwt;
        }

        public ClaimsIdentity GenerateClaimsIdentity<T>(string username, T id, IEnumerable<string> roles) =>
            new ClaimsIdentity(new GenericIdentity(username, "Token"), new Claim[]
                {
                    //new Claim(Helpers.Constants.Strings.JwtClaimIdentifiers.Id, id),
                }
                .Concat(roles.Select(rol => new Claim(CustomClaimTypes.Role, rol))));
    }
}