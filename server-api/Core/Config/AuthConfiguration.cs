﻿namespace Server.Api.Core.Config
{
    public class AuthConfiguration
    {
        public string Issuer { get; set; }
        public string Audience { get; set; }
        public double RefreshTokenLifeTime { get; set; }
        public double JwtTokenLifeTime { get; set; }
        public string SecretKey { get; set; }
    }
}