﻿using Microsoft.AspNetCore.Identity;

namespace Server.Api.Entities
{
    public class ApplicationUser : IdentityUser
    {
    }
}