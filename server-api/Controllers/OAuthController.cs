﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.Extensions.Options;
using Server.Api.Core.Auth;
using Server.Api.Core.Config;
using Server.Api.Core.Utils;
using Server.Api.Database;
using Server.Api.Entities;

namespace Server.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OAuthController : ControllerBase
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly RefreshTokenRepository _tokenRepository;
        private readonly AuthConfiguration _authConfiguration;

        public OAuthController(UserManager<ApplicationUser> userManager, IJwtFactory jwtFactory,
            IOptions<AuthConfiguration> options, RefreshTokenRepository tokenRepository)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _tokenRepository = tokenRepository;
            _authConfiguration = options.Value;
        }

        [HttpPost("token")]
        [Consumes("application/x-www-form-urlencoded")]
        public async Task<ActionResult<TokenResponse>> Auth([FromForm] Credentials credentials) =>
            credentials.grant_type == "password"
                ? await DoPassword(credentials)
                : credentials.grant_type == "refresh_token"
                    ? await DoRefreshTokenAsync(credentials)
                    : BadRequest();

        private async Task<ActionResult<TokenResponse>> DoPassword(Credentials credentials)
        {
            var (identity, user) = await GetClaimsIdentity(credentials.username, credentials.password);
            if (user == null)
            {
                ModelState.TryAddModelError("", "Error, usuario o contraseña inválida.");
                return BadRequest(ModelState);
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");
            var token = GetRefreshToken(credentials.username, refreshTokenId);

            await _tokenRepository.Add(token);

            return Ok(new TokenResponse
            {
                AccessToken = _jwtFactory.GenerateEncodedToken(credentials.username, identity),
                RefreshToken = refreshTokenId,
            });
        }

        private async Task<ActionResult<TokenResponse>> DoRefreshTokenAsync(Credentials credentials)
        {
            var hashedTokenId = CryptoUtils.GetHash(credentials.refresh_token);
            var currentRefreshToken = await _tokenRepository.Find(hashedTokenId);

            if (currentRefreshToken == null || currentRefreshToken.ExpiresUtc <= DateTime.UtcNow)
            {
                ModelState.TryAddModelError("", "El token de refresco es inválido o ha expirado.");
                return BadRequest(ModelState);
            }

            var refreshTokenId = Guid.NewGuid().ToString("n");
            var refreshToken = GetRefreshToken(currentRefreshToken.Subject, refreshTokenId);

            var (identity, _) = await GetClaimsIdentity(currentRefreshToken.Subject);

            await _tokenRepository.Add(refreshToken);

            return Ok(new TokenResponse
            {
                AccessToken = _jwtFactory.GenerateEncodedToken(currentRefreshToken.Subject, identity),
                RefreshToken = refreshTokenId,
            });
        }

        private async Task<(ClaimsIdentity identity, ApplicationUser user)> GetClaimsIdentity(string username,
            string password = null)
        {
            if (string.IsNullOrEmpty(username) || password != null && string.IsNullOrWhiteSpace(password))
                return await Task.FromResult<(ClaimsIdentity identity, ApplicationUser user)>((null, null));

            // get the user to verifty
            var userToVerify = await _userManager.FindByNameAsync(username);

            if (userToVerify == null)
                return await Task.FromResult<(ClaimsIdentity identity, ApplicationUser user)>((null, null));

            var roles = await _userManager.GetRolesAsync(userToVerify);

            // check the credentials
            if (password == null || await _userManager.CheckPasswordAsync(userToVerify, password))
            {
                return await Task.FromResult((_jwtFactory.GenerateClaimsIdentity(username, userToVerify.Id, roles),
                    userToVerify));
            }

            // Credentials are invalid, or account doesn't exist
            return await Task.FromResult<(ClaimsIdentity identity, ApplicationUser user)>((null, null));
        }

        private RefreshToken GetRefreshToken(string username, string refreshTokenId) => new RefreshToken
        {
            Id = CryptoUtils.GetHash(refreshTokenId),
            Subject = username,
            IssuedUtc = DateTime.UtcNow,
            ExpiresUtc = DateTime.UtcNow.AddMinutes(_authConfiguration.RefreshTokenLifeTime),
        };
    }
}